package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private var operand:Double=0.0
    private var operation=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView=findViewById(R.id.resultTextView)

    }
    fun numberClick(clickedView:View)
    {
        if (clickedView is TextView){
            var result=resultTextView.text.toString()
            val number=clickedView.text.toString()
            if (result=="0"){
                result =""
            }
            resultTextView.text=result+number
        }
    }
    fun operationclick(clickedView: View){
        if (clickedView is TextView){
            var operand=resultTextView.text.toString()
            this.operand=operand.toDouble()
            operation=clickedView.text.toString()
            resultTextView.text="0"
        }
    }
    fun equalsonClick(clickedView: View){
        val secondOperand=resultTextView.text.toString().toDouble()
        if (secondOperand.toString() !="0"){
            when(operation){
                "+" -> resultTextView.text=(operand+secondOperand).toString()
                "-" -> resultTextView.text=(operand-secondOperand).toString()
                "x" -> resultTextView.text=(operand*secondOperand).toString()
                "/" -> resultTextView.text=(operand/secondOperand).toString()
            }}
        if(resultTextView.text.toString().takeLast(2)==".0"){
            resultTextView.text=resultTextView.text.toString().dropLast(2)
        }
        operand=0.0
    }

    fun deleteOnClick(clickedView: View){
        val zero="0"
        val text=resultTextView.text.toString()
        if (text.isNotEmpty()){
            if (text!=zero){
                resultTextView.text=text.dropLast(1)
            }
            if(text.length==1){
                resultTextView.text="0"
            }
        }
    }
    fun clearonClick(clickedView: View){
        val text=resultTextView.text.toString()
        val washashleliraodenoba=text.length
        if (text.isNotEmpty()){
            if (text!="0") {
                resultTextView.text = text.drop(washashleliraodenoba) + "0"
            }
        }
    }
    fun onClickmakeDouble(clickedView: View){
        var text=resultTextView.text.toString()
        if("." in  text==false){
            resultTextView.text=text+"."
        }
    }

}